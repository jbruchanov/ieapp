package com.example.ieapp.util;

/**
 * Created by jbruchanov on 02/10/2014.
 * Helper wrapper for out reference pattern
 */
public class OutRef<T> {
    public T value;

    public OutRef(T value) {
        this.value = value;
    }

    public OutRef() {
    }
}

