package com.example.ieapp.twitter;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.example.ieapp.IERunner;
import com.example.ieapp.R;
import com.example.ieapp.twitter.TwitterStreamWrapper.TwitterStreamWrapperListener;
import com.example.ieapp.util.OutRef;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowLooper;

import twitter4j.FilterQuery;
import twitter4j.Status;
import twitter4j.TwitterStream;
import twitter4j.conf.Configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(IERunner.class)
public class TwitterStreamWrapperTest {

    @Test
    public void testConfigurationBuildsWithCredentials() {
        TwitterStreamWrapper wrapper = mock(TwitterStreamWrapper.class);
        doCallRealMethod().when(wrapper).onBuildConfiguration(Matchers.<Context>any());

        Configuration configuration = wrapper.onBuildConfiguration(Robolectric.application);

        assertEquals(getString(R.string.twitter_oauthconsumerkey), configuration.getOAuthConsumerKey());
        assertEquals(getString(R.string.twitter_oauthconsumersecret), configuration.getOAuthConsumerSecret());
        assertEquals(getString(R.string.twitter_oauthaccesstoken), configuration.getOAuthAccessToken());
        assertEquals(getString(R.string.twitter_oauthaccesstokensecret), configuration.getOAuthAccessTokenSecret());
    }

    @Test
    public void testDispatchToListenerWontCrashIfNull(){
        TwitterStreamWrapper wrapper = new TwitterStreamWrapper(Robolectric.application);
        wrapper.dispatchOnException(null);
        wrapper.dispatchOnStatus(null);
    }

    @Test
    public void testDispatchToListenerIfSet(){
        TwitterStreamWrapper wrapper = new TwitterStreamWrapper(Robolectric.application);

        final OutRef<Status> outStatus = new OutRef<>();
        final OutRef<Exception> outEx = new OutRef<>();

        wrapper.addWrapperListener(new TwitterStreamWrapperListener() {
            @Override
            public void onStatus(@NonNull Status status) {
                outStatus.value = status;
            }

            @Override
            public void onException(Exception ex) {
                outEx.value = ex;
            }
        });

        Exception mockEx = mock(Exception.class);
        Status mockStatus = mock(Status.class);

        wrapper.dispatchOnException(mockEx);
        wrapper.dispatchOnStatus(mockStatus);

        assertEquals(mockStatus, outStatus.value);
        assertEquals(mockEx, outEx.value);
    }

    @Test
    public void testStartFilter() {
        final TwitterStream stream = mock(TwitterStream.class);
        TwitterStreamWrapper wrapper = wrapperWithMockStream(stream);

        FilterQuery fq = new FilterQuery();
        wrapper.start(fq);

        verify(stream).filter(eq(fq));
    }

    @Test
    public void testShutdown() {
        final TwitterStream stream = mock(TwitterStream.class);
        TwitterStreamWrapper wrapper = wrapperWithMockStream(stream);

        wrapper.shutdown();

        verify(stream).shutdown();
    }

    @Test
    public void testBindsTwitterStream() throws InterruptedException {
        final Object lock = new Object();
        final OutRef<Boolean> bindingWorks = new OutRef<>(false);
        TwitterStreamWrapper wrapper = new TwitterStreamWrapper(Robolectric.application);
        wrapper.addWrapperListener(new TwitterStreamWrapperListener() {
            @Override
            public void onStatus(@NonNull Status status) {
            }

            @Override
            public void onException(@NonNull Exception ex) {
                bindingWorks.value = true;
                synchronized (lock) {
                    lock.notifyAll();
                }
            }
        });
        wrapper.start(new FilterQuery());
        synchronized (lock) {
            for (int i = 0; i < 10; i++) {//wait up to 10s for expected exception
                lock.wait(1000);
                ShadowLooper sl = Robolectric.shadowOf(Looper.getMainLooper());
                if (sl.getScheduler().advanceToLastPostedRunnable()) {
                    break;
                }
            }
        }

        assertTrue(bindingWorks.value);
    }

    @Test
    public void testIsRunning() {
        TwitterStreamWrapper wrapper = wrapperWithMockStream(mock(TwitterStream.class));
        wrapper.start(new FilterQuery());
        assertTrue(wrapper.isRunning());
    }

    @Test
    public void testIsNotRunning() {
        TwitterStreamWrapper wrapper = wrapperWithMockStream(mock(TwitterStream.class));
        wrapper.start(new FilterQuery());
        wrapper.shutdown();
        assertFalse(wrapper.isRunning());
    }

    private String getString(@StringRes int resId) {
        return Robolectric.application.getString(resId);
    }


    private static TwitterStreamWrapper wrapperWithMockStream(final TwitterStream stream) {
        return new TwitterStreamWrapper(Robolectric.application) {
            @NonNull
            @Override
            protected TwitterStream onCreateTwitterStream(@NonNull Configuration configuration) {
                return stream;
            }
        };
    }
}