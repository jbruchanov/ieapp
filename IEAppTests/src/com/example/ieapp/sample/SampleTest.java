package com.example.ieapp.sample;

import android.support.annotation.StringRes;

import com.example.ieapp.IERunner;
import com.example.ieapp.MainActivity;
import com.example.ieapp.R;
import com.example.ieapp.util.OutRef;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

/**
 * User: jbruchanov
 * Date: 02/10/2014
 */
@RunWith(IERunner.class)
public class SampleTest {

    @Test
    public void testRobolectric() {
        MainActivity activity = Robolectric.buildActivity(MainActivity.class).get();
        assertNotNull(activity);
    }

    @Test
    public void testMock() {
        MainActivity mock = mock(MainActivity.class);
        assertNotNull(mock);
    }

    @Test
    @Ignore //depends on internet, credentials and it's long running test
    public void testTwitter() throws InterruptedException {
        final Object lock = new Object();
        final OutRef<Boolean> outRef = new OutRef<>(false);
        StatusListener listener = new StatusListener(){
            @Override
            public void onStatus(Status status) {
                outRef.value = true;
                assertNotNull(status);
                synchronized (lock) {
                    lock.notifyAll();
                }
            }

            @Override
            public void onException(Exception ex) {
                fail("Unable to connect to twitter");
                ex.printStackTrace();
            }

            @Override public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) { }
            @Override public void onTrackLimitationNotice(int numberOfLimitedStatuses) { }
            @Override public void onScrubGeo(long userId, long upToStatusId) { }
            @Override public void onStallWarning(StallWarning warning) { }
        };

        ConfigurationBuilder cb = new ConfigurationBuilder()
            .setDebugEnabled(true)
            .setOAuthConsumerKey(getString(R.string.twitter_oauthconsumerkey))
            .setOAuthConsumerSecret(getString(R.string.twitter_oauthconsumersecret))
            .setOAuthAccessToken(getString(R.string.twitter_oauthaccesstoken))
            .setOAuthAccessTokenSecret(getString(R.string.twitter_oauthaccesstokensecret));

        Configuration conf = cb.build();

        TwitterStream twitterStream = new TwitterStreamFactory(conf).getInstance();

        FilterQuery query = new FilterQuery();
        query.language(new String[]{"en"});
        query.track(new String[]{"banking"});

        twitterStream.addListener(listener);
        twitterStream.filter(query);

        try {
            synchronized (lock) {
                lock.wait(10000);
            }
        } finally {
            //ignore error
            twitterStream.shutdown();
        }
        assertTrue(outRef.value);
    }

    public String getString(@StringRes int resId) {
        return Robolectric.application.getString(resId);
    }
}
