package com.example.ieapp;

import android.os.Build;

import org.junit.runners.model.InitializationError;
import org.robolectric.AndroidManifest;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.res.Fs;
/**
 * User: jbruchanov
 * Date: 01/10/2014
 * Time: 21:13
 */
public class IERunner extends RobolectricTestRunner {
    public static final int SDK = Build.VERSION_CODES.JELLY_BEAN_MR2;

    public IERunner(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    protected AndroidManifest getAppManifest(Config config) {
        String manifestProperty = "../IEApp/AndroidManifest.xml";
        String resProperty = "../IEApp/res";
        return new AndroidManifest(Fs.fileFromPath(manifestProperty), Fs.fileFromPath(resProperty)) {
            @Override
            public int getTargetSdkVersion() {
                return SDK;
            }
        };
    }
}
