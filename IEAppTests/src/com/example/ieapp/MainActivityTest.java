package com.example.ieapp;

import android.support.annotation.NonNull;
import android.widget.ListAdapter;

import com.example.ieapp.twitter.TwitterStreamWrapper;
import com.example.ieapp.twitter.TwitterStreamWrapper.TwitterStreamWrapperListener;
import com.example.ieapp.widget.TwitterStreamAdapter;

import org.fest.assertions.api.ANDROID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowToast;
import org.robolectric.shadows.ShadowWindow;

import java.util.ArrayList;
import java.util.List;

import twitter4j.FilterQuery;

import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(IERunner.class)
public class MainActivityTest {

    @Test
    public void testActivityInflatesContentAndInitsViews() {
        MainActivity activity = Robolectric.buildActivity(MainActivity.class).create().get();
        assertNotNull(activity.mListView);
    }

    @Test
    public void testCreatesAndAddsAdapterIntoListView() {
        MainActivity activity = Robolectric.buildActivity(MainActivity.class).create().get();
        ListAdapter adapter = activity.mListView.getAdapter();
        assertTrue(adapter instanceof TwitterStreamAdapter);
    }

    @Test
    public void testStartsStreamOnResume() {
        HelpMainActivity activity = Robolectric.buildActivity(HelpMainActivity.class).create().start().resume().get();
        verify(activity.mWrapper).start(Matchers.<FilterQuery>any());
    }

    @Test
    public void testStopsStreamOnPause() {
        HelpMainActivity activity = Robolectric.buildActivity(HelpMainActivity.class).create().start().resume().pause().get();
        verify(activity.mWrapper).shutdown();
    }

    @Test
    public void testShowsPBarOnResume() {
        HelpMainActivity activity = Robolectric.buildActivity(HelpMainActivity.class).create().start().resume().get();
        ShadowWindow sw = Robolectric.shadowOf(activity.getWindow());
        ANDROID.assertThat(sw.getIndeterminateProgressBar()).isVisible();
    }

    @Test
    public void testHidesPBarOnPause() {
        HelpMainActivity activity = Robolectric.buildActivity(HelpMainActivity.class).create().start().resume().pause().get();
        ShadowWindow sw = Robolectric.shadowOf(activity.getWindow());
        ANDROID.assertThat(sw.getIndeterminateProgressBar()).isNotVisible();
    }

    @Test
    public void testHidesPBarOnError() {
        HelpMainActivity activity = Robolectric.buildActivity(HelpMainActivity.class).create().start().resume().get();
        ShadowWindow sw = Robolectric.shadowOf(activity.getWindow());

        for (TwitterStreamWrapperListener listener : activity.mListeners) {
            listener.onException(new Exception("Error"));
        }

        ANDROID.assertThat(sw.getIndeterminateProgressBar()).isNotVisible();
    }

    @Test
    public void testShowsToastOnError() {
        HelpMainActivity activity = Robolectric.buildActivity(HelpMainActivity.class).create().start().resume().get();

        assertNull(ShadowToast.getLatestToast());
        for (TwitterStreamWrapperListener listener : activity.mListeners) {
            listener.onException(new Exception("Error"));
        }
        assertNotNull(ShadowToast.getLatestToast());
    }

    private static class HelpMainActivity extends MainActivity{
        TwitterStreamWrapper mWrapper = mock(TwitterStreamWrapper.class);
        List<TwitterStreamWrapperListener> mListeners = new ArrayList<>();

        private HelpMainActivity() {
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    mListeners.add((TwitterStreamWrapperListener) invocation.getArguments()[0]);
                    return null;
                }
            }).when(mWrapper).addWrapperListener(Matchers.<TwitterStreamWrapperListener>any());
        }

        @NonNull
        @Override
        protected TwitterStreamWrapper onCreateWrapper() {
            return mWrapper;
        }

    }
}