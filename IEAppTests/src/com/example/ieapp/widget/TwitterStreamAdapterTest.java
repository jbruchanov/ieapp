package com.example.ieapp.widget;

import android.annotation.TargetApi;
import android.os.Build.VERSION_CODES;
import android.view.View;
import android.widget.TextView;

import com.example.ieapp.IERunner;
import com.example.ieapp.R;
import com.example.ieapp.twitter.TwitterStreamWrapper;
import com.example.ieapp.twitter.TwitterStreamWrapper.TwitterStreamWrapperListener;
import com.example.ieapp.util.OutRef;
import com.example.ieapp.widget.TwitterStreamAdapter.Tag;

import org.fest.assertions.api.ANDROID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;

import java.text.SimpleDateFormat;
import java.util.Date;

import twitter4j.Status;
import twitter4j.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(IERunner.class)
public class TwitterStreamAdapterTest {

    static final String AUTHOR = "Author";
    static final String MSG = "Msg";
    static final Date CREATED = new Date(100, 1, 1, 12, 0, 0);

    @Test
    public void testAdapterBindsToTwitterStreamWrapper() {
        TwitterStreamWrapper wrapper = mock(TwitterStreamWrapper.class);
        new TwitterStreamAdapter(Robolectric.application, wrapper);

        verify(wrapper).addWrapperListener(Matchers.<TwitterStreamWrapperListener>any());
    }

    @Test
    public void testMaxLengthIsMaxByDefault() {
        TwitterStreamAdapter streamAdapter = new TwitterStreamAdapter(Robolectric.application, mock(TwitterStreamWrapper.class));
        assertEquals(Integer.MAX_VALUE, streamAdapter.getMaxLength());
    }

    @Test
    public void testMaxLength() {
        TwitterStreamAdapter streamAdapter = new TwitterStreamAdapter(Robolectric.application, 3, mock(TwitterStreamWrapper.class));
        assertEquals(3, streamAdapter.getMaxLength());
    }

    @Test
    public void testAddNewTweet() {
        TwitterStreamAdapter streamAdapter = new TwitterStreamAdapter(Robolectric.application, 2, mock(TwitterStreamWrapper.class));
        Status status = mock(Status.class);
        streamAdapter.addTweet(status);

        assertEquals(1, streamAdapter.getCount());
        assertEquals(status, streamAdapter.getItem(0));
    }

    @Test
    public void testMaxLengthDeletesLastValue() {
        TwitterStreamAdapter streamAdapter = new TwitterStreamAdapter(Robolectric.application, 2, mock(TwitterStreamWrapper.class));
        Status[] statuses = {mock(Status.class), mock(Status.class), mock(Status.class), mock(Status.class)};
        for (Status status : statuses) {
            streamAdapter.addTweet(status);
        }

        assertEquals(2, streamAdapter.getCount());

        assertEquals(statuses[statuses.length - 1], streamAdapter.getItem(0));
        assertEquals(statuses[statuses.length - 2], streamAdapter.getItem(1));
    }

    @Test
    public void testAddNewTweetNotifiesDataSetChanged() {
        TwitterStreamAdapter streamAdapter = spy(new TwitterStreamAdapter(Robolectric.application, 2, mock(TwitterStreamWrapper.class)));

        verify(streamAdapter, never()).notifyDataSetChanged();
        streamAdapter.addTweet(mock(Status.class));
        verify(streamAdapter).notifyDataSetChanged();
    }

    @Test
    public void testListenerCallsDispatchMethods() {
        TwitterStreamWrapper mock = mock(TwitterStreamWrapper.class);
        final OutRef<TwitterStreamWrapperListener> listenerOutRef = new OutRef<>();
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                listenerOutRef.value = (TwitterStreamWrapperListener) invocation.getArguments()[0];
                return null;
            }
        }).when(mock).addWrapperListener(Matchers.<TwitterStreamWrapperListener>any());

        final OutRef<Boolean> calledDispatchError = new OutRef<>(false);
        TwitterStreamAdapter streamAdapter = new TwitterStreamAdapter(Robolectric.application, 2, mock) {
            @Override
            protected void dispatchError(Exception er) {
                calledDispatchError.value = true;
            }
        };

        Status status = mock(Status.class);
        listenerOutRef.value.onStatus(status);
        assertEquals(status, streamAdapter.getItem(0));

        listenerOutRef.value.onException(mock(Exception.class));
        assertTrue(calledDispatchError.value);
    }

    @TargetApi(VERSION_CODES.DONUT)
    @Test
    public void testInflatesView() {
        TwitterStreamAdapter adapter = new TwitterStreamAdapter(Robolectric.application, mock(TwitterStreamWrapper.class));
        adapter.addTweet(buildTweet("Author", "Msg", new Date(100, 1, 1, 12, 0, 0)));
        View view = adapter.getView(0, null, null);

        assertNotNull(view.findViewById(R.id.author));
        assertNotNull(view.findViewById(R.id.message));
        assertNotNull(view.findViewById(R.id.time));
        Tag tag = (Tag) view.getTag(R.id.tag_list_item_tweet);
        assertNotNull(tag);

        assertNotNull(tag.author);
        assertNotNull(tag.time);
        assertNotNull(tag.message);
    }

    @Test
    public void testInflatesViewAndSetsValues() {
        TwitterStreamAdapter adapter = new TwitterStreamAdapter(Robolectric.application, mock(TwitterStreamWrapper.class));

        Status tweet = buildTweet(AUTHOR, MSG, CREATED);
        adapter.addTweet(tweet);
        View view = adapter.getView(0, null, null);

        ANDROID.assertThat((TextView)view.findViewById(R.id.author)).hasTextString(AUTHOR);
        ANDROID.assertThat((TextView)view.findViewById(R.id.message)).hasTextString(MSG);
        ANDROID.assertThat((TextView)view.findViewById(R.id.time)).hasTextString(SimpleDateFormat.getDateTimeInstance().format(CREATED));
    }

    @Test
    public void testRecycles() {
        TwitterStreamAdapter adapter = new TwitterStreamAdapter(Robolectric.application, mock(TwitterStreamWrapper.class));
        adapter.addTweet(buildTweet("", "", new Date(0, 2, 3, 4, 5, 6)));
        adapter.addTweet(buildTweet(AUTHOR, MSG, CREATED));
        View view = adapter.getView(1, null, null);
        View newView = adapter.getView(0, view, null);

        assertTrue(newView == view);

        ANDROID.assertThat((TextView)newView.findViewById(R.id.author)).hasTextString(AUTHOR);
        ANDROID.assertThat((TextView)newView.findViewById(R.id.message)).hasTextString(MSG);
        ANDROID.assertThat((TextView)newView.findViewById(R.id.time)).hasTextString(SimpleDateFormat.getDateTimeInstance().format(CREATED));
    }

    private Status buildTweet(String author, String message, Date created) {
        Status status = mock(Status.class);
        User user = mock(User.class);

        doReturn(author).when(user).getName();
        doReturn(user).when(status).getUser();
        doReturn(message).when(status).getText();
        doReturn(created).when(status).getCreatedAt();
        return status;
    }
}