package com.example.ieapp.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ieapp.R;
import com.example.ieapp.twitter.TwitterStreamWrapper;
import com.example.ieapp.twitter.TwitterStreamWrapper.TwitterStreamWrapperListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import twitter4j.Status;

/**
 * Created by jbruchanov on 02/10/2014.
 *
 * AdapterView's Adapter for showing Tweets from {@link TwitterStreamWrapper}
 */
public class TwitterStreamAdapter extends BaseAdapter {

    private final Context mContext;
    private final LinkedList<Status> mDataSet = new LinkedList<>();
    private final DateFormat mDateFormat = SimpleDateFormat.getDateTimeInstance();

    private int mMaxLength = Integer.MAX_VALUE;

    public TwitterStreamAdapter(@NonNull Context context, @NonNull TwitterStreamWrapper wrapper) {
        this(context, Integer.MAX_VALUE, wrapper);
    }

    public TwitterStreamAdapter(@NonNull Context context, int maxLength, @NonNull TwitterStreamWrapper wrapper) {
        mContext = context;
        mMaxLength = maxLength;
        wrapper.addWrapperListener(mWrapperListener);
    }

    @Override
    public int getCount() {
        return mDataSet.size();
    }

    @Override
    @NonNull
    public Status getItem(int position) {
        return mDataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Tag tag;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.list_item_tweet, null);
            convertView.setTag(R.id.tag_list_item_tweet, tag = new Tag());
            tag.author = (TextView) convertView.findViewById(R.id.author);
            tag.time = (TextView) convertView.findViewById(R.id.time);
            tag.message = (TextView) convertView.findViewById(R.id.message);
        } else {
            tag = (Tag) convertView.getTag(R.id.tag_list_item_tweet);
        }

        Status status = getItem(position);
        String name = status.getUser().getName();
        String text = status.getText();
        String createdAt = mDateFormat.format(status.getCreatedAt());

        tag.author.setText(name);
        tag.message.setText(text);
        tag.time.setText(createdAt);
        convertView.setContentDescription(mContext.getString(R.string.a11y_list_item_tweet, name, text, createdAt));
        return convertView;
    }

    /**
     * Add new tweet into dataset.<br/>
     * If dataset exceed {@link #getMaxLength()}, last value is removed<br/>
     * Calls {@link #notifyDataSetChanged()}
     * @param status
     */
    public void addTweet(@NonNull Status status){
        mDataSet.addFirst(status);
        if (mDataSet.size() > mMaxLength) {
            mDataSet.removeLast();
        }
        notifyDataSetChanged();
    }

    /**
     * Get current maximum length to keep
     * @return
     */
    public int getMaxLength() {
        return mMaxLength;
    }

    /**
     * Deliver new tweet into dataset, called before {@link #addTweet(twitter4j.Status)}
     * @param status
     */
    protected void dispatchAddTweet(Status status) {
        addTweet(status);
    }

    /**
     * Called if error in {@link com.example.ieapp.twitter.TwitterStreamWrapper}
     * @param er
     */
    protected void dispatchError(Exception er) {
        //do nothing
    }

    //region helpers
    private final TwitterStreamWrapperListener mWrapperListener = new TwitterStreamWrapperListener() {
        @Override public void onStatus(@NonNull Status status) { dispatchAddTweet(status); }
        @Override public void onException(@NonNull Exception ex) { dispatchError(ex); }
    };

    /**
     * ViewHolder pattern helper
     */
    static class Tag {
        TextView author;
        TextView message;
        TextView time;
    }
    //endregion
}
