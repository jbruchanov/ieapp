package com.example.ieapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ieapp.twitter.TwitterStreamWrapper;
import com.example.ieapp.twitter.TwitterStreamWrapper.TwitterStreamWrapperListener;
import com.example.ieapp.widget.TwitterStreamAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;
import twitter4j.FilterQuery;
import twitter4j.Status;

public class MainActivity extends Activity {

    @InjectView(R.id.list_view)
    ListView mListView;

    private TwitterStreamWrapper mTwitterStreamWrapper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        ButterKnife.inject(this);

        init();
    }

    private void init() {
        mTwitterStreamWrapper = onCreateWrapper();
        mTwitterStreamWrapper.addWrapperListener(mWrapperListener);
        mListView.setAdapter(new TwitterStreamAdapter(this, getResources().getInteger(R.integer.twitter_stream_max_records), mTwitterStreamWrapper));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTwitterStreamWrapper.start(onBuildFilterQuery());
        setProgressBarIndeterminateVisibility(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTwitterStreamWrapper.shutdown();
        setProgressBarIndeterminateVisibility(false);
    }

    /**
     * Build filter query for {@link com.example.ieapp.twitter.TwitterStreamWrapper#start(twitter4j.FilterQuery)}
     * @return
     */
    protected FilterQuery onBuildFilterQuery() {
        FilterQuery fq = new FilterQuery();
        fq.language(new String[]{"en"});
        fq.track(new String[]{"banking"});
        return fq;
    }

    /**
     * Create {@link TwitterStreamWrapper} for ListView's {@link com.example.ieapp.widget.TwitterStreamAdapter}
     * @return
     */
    @NonNull
    protected TwitterStreamWrapper onCreateWrapper() {
        return new TwitterStreamWrapper(this);
    }

    //region helper
    private TwitterStreamWrapperListener mWrapperListener = new TwitterStreamWrapperListener() {
        @Override public void onStatus(@NonNull Status status) { /*DoNothing*/ }

        @Override
        public void onException(@NonNull Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
            setProgressBarIndeterminateVisibility(false);
        }
    };
    //endregion
}
