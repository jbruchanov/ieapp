package com.example.ieapp.twitter;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.ieapp.R;

import java.util.HashSet;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by jbruchanov on 02/10/2014.
 * <p/>
 * Main TwitterStream wrapper for easier work
 */
public class TwitterStreamWrapper {

    /**
     * Basic listener interface for getting tweets
     */
    public interface TwitterStreamWrapperListener {
        /**
         * New tweet callback
         *
         * @param status
         */
        void onStatus(@NonNull Status status);

        /**
         * Error callback
         *
         * @param ex
         */
        void onException(@NonNull Exception ex);
    }

    private static final boolean DEBUG = false;

    private final TwitterStream mTwitterStream;
    private final HashSet<TwitterStreamWrapperListener> mWrapperListeners = new HashSet<>();
    private StatusListener mStreamListener = null;
    private boolean mIsRunning;

    public TwitterStreamWrapper(@NonNull Context context) {
        mTwitterStream = onCreateTwitterStream(onBuildConfiguration(context));
    }

    /**
     * Build configuration for {@link twitter4j.TwitterStream}
     *
     * @param context
     * @return
     */
    @NonNull
    protected Configuration onBuildConfiguration(@NonNull Context context) {
        return new ConfigurationBuilder()
                .setDebugEnabled(DEBUG)
                .setOAuthConsumerKey(context.getString(R.string.twitter_oauthconsumerkey))
                .setOAuthConsumerSecret(context.getString(R.string.twitter_oauthconsumersecret))
                .setOAuthAccessToken(context.getString(R.string.twitter_oauthaccesstoken))
                .setOAuthAccessTokenSecret(context.getString(R.string.twitter_oauthaccesstokensecret))
                .build();
    }

    /**
     * Create {@link twitter4j.TwitterStream}
     * @param configuration
     * @return
     */
    @NonNull
    protected TwitterStream onCreateTwitterStream(@NonNull Configuration configuration) {
        TwitterStream instance = new TwitterStreamFactory(configuration).getInstance();
        instance.addListener(mStreamListener);
        return instance;
    }

    /**
     * Attach listener
     * @param wrapperListener
     */
    public void addWrapperListener(@NonNull TwitterStreamWrapperListener wrapperListener) {
        mWrapperListeners.add(wrapperListener);
    }

    /**
     * Deattach listener
     * @param wrapperListener
     */
    public void removeWrapperListener(@NonNull TwitterStreamWrapperListener wrapperListener) {
        mWrapperListeners.remove(wrapperListener);
    }

    /**
     * Start stream with particular query
     * @param query
     */
    public void start(@NonNull FilterQuery query) {
        mTwitterStream.filter(query);
        mIsRunning = true;
    }

    /**
     * Shutdown current stream
     */
    public void shutdown() {
        mTwitterStream.shutdown();
        mIsRunning = false;
    }

    /**
     * Returns if wrapper is running
     * @return
     */
    public boolean isRunning() {
        return mIsRunning;
    }

    /**
     * Called on new tweet
     * @param status
     */
    protected void dispatchOnStatus(Status status) {
        for (TwitterStreamWrapperListener wrapperListener : mWrapperListeners) {
            wrapperListener.onStatus(status);
        }
    }

    /**
     * Called on error from {@link twitter4j.TwitterStream}
     * @param ex
     */
    protected void dispatchOnException(Exception ex) {
        for (TwitterStreamWrapperListener wrapperListener : mWrapperListeners) {
            wrapperListener.onException(ex);
        }
        shutdown();
    }

    private Handler mHandler = new Handler(Looper.getMainLooper());
    //region helpers
    {
        //TODO: optimization possibility, some smartet data interchange to avoid creating Runnable everytime
        mStreamListener = new StatusListener() {
            @Override
            public void onStatus(final Status status) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        dispatchOnStatus(status);
                    }
                });
            }

            @Override
            public void onException(final Exception ex) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        dispatchOnException(ex);
                    }
                });
            }

            @Override public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) { }
            @Override public void onTrackLimitationNotice(int numberOfLimitedStatuses) { }
            @Override public void onScrubGeo(long userId, long upToStatusId) { }
            @Override public void onStallWarning(StallWarning warning) { }
        };
    }
    //endregion
}
